package com.example.lula.conversordetemperatura;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.Toast.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText centigrados, fahrenheit;
    Button ConvertirCentigrados, ConvertirFahrenheit;
    TextView DatosCentigrados, DatosFarenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        centigrados = (EditText)findViewById(R.id.TXTCenti);
        fahrenheit = (EditText)findViewById(R.id.TXTFahren);
        ConvertirCentigrados = (Button) findViewById(R.id.BTNFahren);
        ConvertirFahrenheit = (Button)findViewById(R.id.BTNCenti);
        DatosCentigrados = (TextView)findViewById(R.id.LBLCenti);
        DatosFarenheit =(TextView)findViewById(R.id.LBLFaren);

        ConvertirFahrenheit.setOnClickListener(this);
        ConvertirCentigrados.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNFahren:
                if (fahrenheit.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.Validacion), Toast.LENGTH_LONG).show();
                }else {
                    Double Fahren = Double.valueOf(fahrenheit.getText().toString());
                    Double Centi = (Fahren - 32) / 1.8;
                    Toast.makeText(MainActivity.this, String.valueOf(Centi)+ " ºC", Toast.LENGTH_LONG).show();
                    DatosCentigrados.setText(String.valueOf(Centi)+ " ºC");
                    fahrenheit.setText("");
                }
                break;

            case R.id.BTNCenti:
                if (centigrados.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.Validacion), Toast.LENGTH_LONG).show();
                }else{
                    Double Centig = Double.valueOf(centigrados.getText().toString());
                    Double Fahrenh = (Centig *1.8 ) + 32;
                    Toast.makeText(MainActivity.this, String.valueOf(Fahrenh)+ " ºF", Toast.LENGTH_LONG).show();
                    DatosFarenheit.setText(String.valueOf(Fahrenh)+ " ºF");
                    centigrados.setText("");
                }
                break;

        }
    }
}
